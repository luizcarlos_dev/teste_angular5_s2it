import {Component, ElementRef, HostListener, Inject, OnInit, ViewChild} from '@angular/core';
import {PeopleService} from "../services/api/people.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";
import {VehiclesService} from "../services/api/vehicles.service";
import {SpeciesService} from "../services/api/species.service";
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {FilmsService} from "../services/api/films.service";
import {Router} from "@angular/router";
import AppLocalStorageService from "../services/app-local-storage-service";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-quiz',
  providers: [],
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  public maxPage = 9;
  public seconds;
  public persons = [];
  public answers = [];
  public page = 1;
  public nextRequest = '';
  public progressDialogRef;
  public finishingDialogRef;
  public answerDialogRef;
  public detailsDialogRef;
  public points = 0;
  public divWidth;
  public cols;
  public secondsInterval;

  @ViewChild('widgetParentDiv') parentDiv:ElementRef;
  @HostListener('window:resize')
  onResize() {
    this._setCols();
  }

  constructor(public _peopleService: PeopleService,
              private _router: Router,
              public snackBar: MatSnackBar,
              public dialog: MatDialog,
              private _appLocalStorageService: AppLocalStorageService) {
  }

  ngOnInit() {
    this._setTime();
    this._setCols();

    this.points = this._appLocalStorageService.getPoints();
    this.page = this._appLocalStorageService.getPage();
    this.answers = this._appLocalStorageService.getAnswers();

    this._fetchPersons();
  }


  /**
   * Método que seta o número de colunas do grip dependendo do tamanho da tela
   * @private
   */
  private _setCols() {
    if(this.parentDiv) {
      this.divWidth = this.parentDiv.nativeElement.clientWidth;
      const cols = parseInt((this.divWidth / 350).toString());
      this.cols = cols > 0 ? cols : 1;
    }
  }


  /**
   * Método que direciona para a proxima página do quiz
   * @public
   */
  next() {
    this.page++;
    this._fetchPersons();
    this.answers = [];
    this._appLocalStorageService.setAnswers(this.answers);
  }

  /**
   * Método que finaliza o quiz
   * @public
   */
  finish() {
    clearInterval(this.secondsInterval);
    this.openFinishingDialog();
  }

  /**
   * Método que criar a uri da imagem a partir do nome do personagem
   * @param name
   * @public
   */
  createPersonImageUri(name) {
    const prefix = '/assets/images/characters/';
    const sufix = '.jpg';
    return prefix + name.split(' ').join('-') + sufix;
  }

  /**
   * Método que busca os personagens da api pela página
   * @private
   */
  private _fetchPersons() {

    this.openProgressDialog();
    this._peopleService
      .findAll('page=' + this.page)
      .subscribe(
        res => {
          this.progressDialogRef.close();
          this.persons = res.results;
          this.nextRequest = res.next;

          this._appLocalStorageService.setPage(this.page);

        },
        err => {
          this.progressDialogRef.close();
        }
      );
  }

  /**
   * Método que seta o tempo inicial do cronometro de tempo limite
   * @private
   */
  private _setTime() {

    this.seconds = this._appLocalStorageService.getSeconds();

    this.secondsInterval = setInterval(() => {
      this.seconds--;
      this._appLocalStorageService.setSeconds(this.seconds);
      if (this.seconds < 0){
        clearInterval(this.secondsInterval);
        this.openFinishingDialog();
      }

    }, 1000);
  }

  /**
   * Método que exibe o dialogo de finalização
   * @public
   */
  openFinishingDialog(): void {
    this.finishingDialogRef = this.dialog.open(FinishingDialog, {
      data: { points: this.points, form: ''},
      disableClose: true
    });

    this.finishingDialogRef.afterClosed().subscribe(data => {

      if (data){

        const ranking = this._appLocalStorageService.getRanking();
        ranking.push({name: data.name, email: data.email, points: this.points, data: new Date()});

        this._appLocalStorageService.setRanking(ranking);

        this._router.navigate(['/start']);
      }

      this.finishingDialogRef.close();

    });
  }

  /**
   * Método que exibe o dialogo de aguarde entre as requisições
   * @public
   */
  openProgressDialog(): void {
    this.progressDialogRef = this.dialog.open(ProgressDialog, {
      width: '200px',
      height: '200px',
      disableClose: true
    });
  }

  /**
   * Método que exibe o snack bar com alguma informação
   * @param message
   * @public
   */
  openSnackBar(message: string) {
    this.snackBar.open(message, 'OK', {
      duration: 5000,
    });
  }

  /**
   * Método que exibe o dialogo de resposta
   * @public
   */
  openAnswerDialog(person, position) {

    this.answerDialogRef = this.dialog.open(AnswerDialog, {
      disableClose: true,
      data: { person: person, answer: ''}
    });

    this.answerDialogRef.afterClosed().subscribe(answer => {

      if (answer){

        this.answers[position] = {...this.answers[position], answered : true};

        let formatedAnswer = answer.split(' ').join('').toLowerCase();
        formatedAnswer = formatedAnswer.split('-').join('');

        let correctAnswer = this.persons[position].name.split(' ').join('').toLowerCase();
        correctAnswer = correctAnswer.split('-').join('');
        if (formatedAnswer === correctAnswer){

          if (this.answers[position].askedDetails){
            this.points += 5;
          }else{
            this.points += 10;
          }

          this._appLocalStorageService.setPoints(this.points);

          this.openSnackBar('Resposta Correta');
        }else{
          this.openSnackBar('Resposta Incorreta');
        }

        this._appLocalStorageService.setAnswers(this.answers);

      }
    });
  }

  /**
   * Método que exibe o dialogo de detalhes
   * @public
   */
  openDetailsDialog(person, position) {

    this.answers[position] = {...this.answers[position], askedDetails : true};

    this.detailsDialogRef = this.dialog.open(DetailsDialog, {
      disableClose: true,
      width: '1200px',
      data: { person: person}
    });

  }

}

@Component({
  selector: 'app-progress-dialog',
  templateUrl: 'progress-dialog.html',
  styleUrls: ['./quiz.component.scss']
})
export class ProgressDialog {

  constructor(public dialogRef: MatDialogRef<ProgressDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'app-answer-dialog',
  templateUrl: 'answer-dialog.html',
  styleUrls: ['./quiz.component.scss']
})
export class AnswerDialog {

  constructor(public dialogRef: MatDialogRef<ProgressDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  /**
   * Método que criar a uri da imagem a partir do nome do personagem
   * @param name
   * @public
   */
  createPersonImageUri(name) {
    const prefix = '/assets/images/characters/';
    const sufix = '.jpg';
    return prefix + name.split(' ').join('-') + sufix;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'app-finishing-dialog',
  templateUrl: 'finishing-dialog.html',
  styleUrls: ['./quiz.component.scss']
})
export class FinishingDialog {

  nameFormControl = new FormControl('', [
    Validators.required
  ]);

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(public dialogRef: MatDialogRef<FinishingDialog>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

  }
}

@Component({
  selector: 'app-details-dialog',
  templateUrl: 'details-dialog.html',
  styleUrls: ['./quiz.component.scss']
})
export class DetailsDialog implements OnInit{

  public films = [];
  public vehicles = [];
  public species = [];

  constructor(public dialogRef: MatDialogRef<ProgressDialog>,
              public _filmsService: FilmsService,
              public _vehiclesService: VehiclesService,
              public _speciesService: SpeciesService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.films = [];
    this.vehicles = [];
    this.species = [];

    for (const url of this.data.person.films){
      this._fetchFilm(url);
    }

    for (const url of this.data.person.vehicles){
      this._fetchVehicles(url);
    }

    for (const url of this.data.person.species){
      this._fetchSpecies(url);
    }

  }

  /**
   * Método que busca a espécie do personagem por url
   * @param url
   * @private
   */
  private _fetchSpecies(url: string) {
    this._speciesService
      .findByCustomUrl(url)
      .subscribe(
        res => {
          this.species.push(res);
        },
        err => {
        }
      );
  }

  /**
   * Método que busca os veículos do personagem por url
   * @param url
   * @private
   */
  private _fetchVehicles(url: string) {
    this._vehiclesService
      .findByCustomUrl(url)
      .subscribe(
        res => {
          this.vehicles.push(res);
        },
        err => {
        }
      );
  }

  /**
   * Método que busca os filmes do personagem por url
   * @param url
   * @private
   */
  private _fetchFilm(url: string) {
    this._filmsService
      .findByCustomUrl(url)
      .subscribe(
        res => {
          this.films.push(res);
        },
        err => {
        }
      );
  }

  /**
   * Método que criar a uri da imagem a partir do nome do personagem
   * @param name
   * @public
   */
  createPersonImageUri(name) {
    const prefix = '/assets/images/characters/';
    const sufix = '.jpg';
    return prefix + name.split(' ').join('-') + sufix;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    this.dialogRef.close();
  }

}
