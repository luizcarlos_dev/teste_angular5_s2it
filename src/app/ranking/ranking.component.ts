import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {default as AppLocalStorageService} from "../services/app-local-storage-service";
import * as moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-BR');

@Component({
  selector: 'app-ranking',
  providers: [],
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

  public ranking = [];

  constructor(private _router: Router, private _appLocalStorageService: AppLocalStorageService) {
  }

  /**
   * Retorna a data informada com uma formatação mais natural
   * @param data
   * @public
   */
  formatData = data => {
    return moment(data)
      .format('LL');
  };

  ngOnInit() {
    this.getRanking();
  }

  /**
   * Método que obtem o ranking armazenado no localstorage
   * @public
   */
  private getRanking() {
    const ranking = this._appLocalStorageService.getRanking();
    this.ranking = ranking.sort((a, b) => a.points < b.points ? 1 : a.points > b.points ? -1 : 0);
  }

}
