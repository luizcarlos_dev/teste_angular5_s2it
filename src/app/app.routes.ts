import {Routes} from '@angular/router';
import {StartComponent} from './start/start.component';
import {QuizComponent} from "./quiz/quiz.component";
import {RankingComponent} from "./ranking/ranking.component";

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'start',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'start',
        component: StartComponent
      },
      {
        path: 'quiz',
        component: QuizComponent
      },
      {
        path: 'ranking',
        component: RankingComponent
      }
    ]
  },
];
