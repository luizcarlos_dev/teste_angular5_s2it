import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {QUIZ_TIME_IN_SECONDS} from '../../constantes';
import {default as AppLocalStorageService} from "../services/app-local-storage-service";

@Component({
  selector: 'app-start',
  providers: [],
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {

  constructor(private _router: Router, private _appLocalStorageService: AppLocalStorageService) {
  }

  ngOnInit() {

  }

  /**
   * Inicia o quiz
   * @public
   */
  public start() {

    this._appLocalStorageService.setSeconds(QUIZ_TIME_IN_SECONDS);
    this._appLocalStorageService.setPage(1);
    this._appLocalStorageService.setPoints(0);
    this._appLocalStorageService.setAnswers([]);

    this._router.navigate(['/quiz']);

  }

  /**
   * Exibir o ranking atual
   * @public
   */
  public ranking() {
    this._router.navigate(['/ranking']);
  }


}
