import {QUIZ_TIME_IN_SECONDS} from "../../constantes";
import {Injectable} from "@angular/core";

@Injectable()
class AppLocalStorageService {

  constructor () {
  }

  /**
   * Método que obtem o ranking armazena no localstorage
   * @public
   */
  getRanking() {
    const ranking = localStorage.getItem('ranking');
    return ranking
      ? JSON.parse(ranking)
      : []
  }
  /**
   * Método que obtem os segundos armazena no localstorage
   * @public
   */
  getSeconds() {
    const seconds = localStorage.getItem('seconds');
    return seconds
      ? JSON.parse(seconds)
      : QUIZ_TIME_IN_SECONDS
  }

  /**
   * Método que obtem a página armazenada no localstorage
   * @public
   */
  getPage() {
    const page = localStorage.getItem('page');
    return page
      ? JSON.parse(page)
      : 1
  }

  /**
   * Método que obtem os pontos armazenados no localstorage
   * @public
   */
  getPoints() {
    const points = localStorage.getItem('points');
    return points
      ? JSON.parse(points)
      : 0
  }

  /**
   * Método que obtem as respostas armazena no localstorage
   * @public
   */
  getAnswers() {
    const answers = localStorage.getItem('answers');
    return answers
      ? JSON.parse(answers)
      : []
  }

  /**
   * Método que armazena os segundos no localstorage
   * @param seconds
   * @public
   */
  setSeconds(seconds) {
    localStorage.setItem('seconds', JSON.stringify(seconds));
  }

  /**
   * Método que armazena a página no localstorage
   * @param page
   * @public
   */
  setPage(page) {
    localStorage.setItem('page', JSON.stringify(page));
  }

  /**
   * Método que armazena os pontos no localstorage
   * @param points
   * @public
   */
  setPoints(points) {
    localStorage.setItem('points', JSON.stringify(points));
  }

  /**
   * Método que armazena as pessoas no localstorage
   * @param answers
   * @public
   */
  setAnswers(answers) {
    localStorage.setItem('answers', JSON.stringify(answers));
  }

  /**
   * Método que armazena o ranking no localstorage
   * @param ranking
   * @public
   */
  setRanking(ranking) {
    localStorage.setItem('ranking', JSON.stringify(ranking));
  }

}

export default AppLocalStorageService;
