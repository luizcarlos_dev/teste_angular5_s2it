import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class SpeciesService extends ApiService {

  constructor (http: Http) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'species');
  }

  /**
   *Obter espécies da api por id
   * @returns {Observable<any|any>}
   */
  findById(id: string) {
    return this.get(id, this.options);
  }

  /**
   *Obter espécies da url customizada
   * @returns {Observable<any|any>}
   */
  findByCustomUrl(url: string) {
    return this.mapResponse(this.http.get(url, this.options));
  }


}

