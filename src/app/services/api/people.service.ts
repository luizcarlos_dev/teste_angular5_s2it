import ApiService from './api.service';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class PeopleService extends ApiService {

  constructor (http: Http) {
    const apiUrl = environment.api.default.uri;
    super(http, apiUrl + 'people');
  }

  /**
   *Obter todas  pessoas da api
   * @returns {Observable<any|any>}
   */
  findAll(query?: string) {
    return this.get('?' + query, this.options);
  }

}

