import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AppRoutes} from './app.routes';
import {StartComponent} from './start/start.component';
import {AnswerDialog, DetailsDialog, FinishingDialog, ProgressDialog, QuizComponent} from './quiz/quiz.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MaterialModule} from './material.module';
import {PeopleService} from "./services/api/people.service";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {VehiclesService} from "./services/api/vehicles.service";
import {FilmsService} from "./services/api/films.service";
import {SpeciesService} from "./services/api/species.service";
import AppLocalStorageService from "./services/app-local-storage-service";
import {RankingComponent} from "./ranking/ranking.component";

@NgModule({
  entryComponents: [
    ProgressDialog,
    AnswerDialog,
    DetailsDialog,
    FinishingDialog
  ],
  declarations: [
    AppComponent,
    StartComponent,
    QuizComponent,
    RankingComponent,
    ProgressDialog,
    AnswerDialog,
    DetailsDialog,
    FinishingDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(AppRoutes),
    MaterialModule,
    BrowserAnimationsModule,
    HttpModule
  ],
  providers: [
    AppLocalStorageService,
    PeopleService,
    FilmsService,
    VehiclesService,
    SpeciesService
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
