export const environment = {

  production: false,

  api: {
    default: {
      uri: 'https://swapi.co/api/'
    }
  }

};
